package algo.huffman;

public class LeafNode<T> extends Node<T> {
    public LeafNode(T item) {
        super(item, null, null);
    }

    @Override
    public boolean isLeaf() {
        return true;
    }
}
