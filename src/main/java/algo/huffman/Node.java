package algo.huffman;

import lombok.Data;

@Data
public class Node<T> {
    private final T item;
    private final Node<T> left;
    private final Node<T> right;

    public boolean isLeaf() {
        return false;
    }
}
