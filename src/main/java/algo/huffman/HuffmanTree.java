package algo.huffman;

import lombok.Value;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.io.IOException;
import java.util.Map;

@Value
public class HuffmanTree {
    private final Node<Map.Entry<String, Integer>> rootNode;

    public void write(BitOutputStream bitOutputStream) throws IOException {
        write(bitOutputStream, rootNode);
    }

    private void write(BitOutputStream bitOutputStream, Node<Map.Entry<String, Integer>> node) throws IOException {
        if (node.isLeaf()) {
            bitOutputStream.writeBit(1);
            String text = node.getItem().getKey();
            for (char character : text.toCharArray()) {
                bitOutputStream.writeCharacter(character);
            }
        } else  {
            bitOutputStream.writeBit(0);
            write(bitOutputStream, node.getLeft());
            write(bitOutputStream, node.getRight());
        }
    }

    public static HuffmanTree loadTree(int encodedCharactersLength, BitInputStream bitInputStream) throws IOException {
        Node<Map.Entry<String, Integer>> rootNode = loadNode(encodedCharactersLength, bitInputStream);
        return new HuffmanTree(rootNode);
    }

    private static Node<Map.Entry<String, Integer>> loadNode(int encodedCharactersLength, BitInputStream bitInputStream) throws IOException {
        boolean isLeaf = bitInputStream.readBit() == 1;
        if (isLeaf) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < encodedCharactersLength; i++) {
                char character = (char) bitInputStream.readCharacter();
                stringBuilder.append(character);
            }
            return new LeafNode<>(new ImmutablePair<>(stringBuilder.toString(), -1));
        } else {
            return new Node<>(new ImmutablePair<>(null, -1),
                    loadNode(encodedCharactersLength, bitInputStream),
                    loadNode(encodedCharactersLength, bitInputStream));
        }
    }
}
