package algo.huffman;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.io.OutputStream;

public class BitOutputStream implements AutoCloseable {
    private OutputStream outputStream;
    private int currentByte;
    private int numBitsInCurrentByte;

    public BitOutputStream(OutputStream outputStream) {
        Preconditions.checkNotNull(outputStream);
        this.outputStream = outputStream;
        currentByte = 0;
        numBitsInCurrentByte = 0;
    }

    public void writeBit(int bit) throws IOException {
        Preconditions.checkArgument(bit == 0 || bit == 1);
        currentByte = currentByte << 1 | bit;
        numBitsInCurrentByte++;
        if (numBitsInCurrentByte == 8) {
            outputStream.write(currentByte);
            numBitsInCurrentByte = 0;
        }
    }

    public void writeInt(int integer) throws IOException {
        int[] reverseIntRepresentation = new int[4];
        for (int i = 0; i < 4; i++) {
            reverseIntRepresentation[i] = integer % 256;
            integer = integer / 256;
        }
        for (int i = 3; i >= 0; i--) {
            writeByte(reverseIntRepresentation[i]);
        }
    }

    public void writeByte(int byteNum) throws IOException {
        writeCharacter(byteNum);
    }

    public void writeCharacter(int character) throws IOException {
        Preconditions.checkArgument(character < 256);
        int[] reverseByteRepresentation = new int[8];
        for (int i = 0; i < 8; i++) {
            reverseByteRepresentation[i] = character % 2;
            character = character / 2;
        }
        for (int i = 7; i >= 0; i--) {
            writeBit(reverseByteRepresentation[i]);
        }
    }

    public void close() throws IOException {
        while (numBitsInCurrentByte != 0) {
            writeBit(0);
        }
        outputStream.close();
    }
}
