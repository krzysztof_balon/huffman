package algo.huffman;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class HuffmanApplication {
    private static final Charset CHARSET = Charset.forName("UTF-8");

    public static void main(String[] args) throws IOException {
        Triple<Integer, String, String> arguments = parseArguments(args);
        int encodedCharsLength = arguments.getLeft();
        String inputText = Files.toString(new File(arguments.getMiddle()), CHARSET);
        String outputFileName = arguments.getRight();

        Set<Map.Entry<String, Integer>> groupedByFrequencies = new HuffmanGrouper().group(encodedCharsLength, inputText);
        List<Node<Map.Entry<String, Integer>>> groupFrequencyNodes = groupedByFrequencies.parallelStream()
                .map(LeafNode::new).collect(Collectors.toList());

        Comparator<Node<Map.Entry<String, Integer>>> frequencyComparator
                = (node1, node2) -> node1.getItem().getValue() - node2.getItem().getValue();

        while (groupFrequencyNodes.size() > 1) {
            groupFrequencyNodes.sort(frequencyComparator);
            Node<Map.Entry<String, Integer>> charGroupWithMinFreq1 = groupFrequencyNodes.get(0);
            Node<Map.Entry<String, Integer>> charGroupWithMinFreq2 = groupFrequencyNodes.get(1);

            int newGroupFreq = charGroupWithMinFreq1.getItem().getValue() + charGroupWithMinFreq2.getItem().getValue();
            Node<Map.Entry<String, Integer>> parentNode = new Node<>(
                    new ImmutablePair<>(null, newGroupFreq), charGroupWithMinFreq1, charGroupWithMinFreq2);

            groupFrequencyNodes.remove(0);
            groupFrequencyNodes.remove(0);
            groupFrequencyNodes.add(parentNode);
        }

        System.out.println(groupFrequencyNodes.get(0));
        try (BitOutputStream bitOutputStream = new BitOutputStream(new BufferedOutputStream(new FileOutputStream(outputFileName)))) {
            bitOutputStream.writeByte(encodedCharsLength);
            HuffmanTree huffmanTree = new HuffmanTree(groupFrequencyNodes.get(0));
            huffmanTree.write(bitOutputStream);
            bitOutputStream.writeInt(inputText.length());

            HuffmanEncoder huffmanEncoder = new HuffmanEncoder(huffmanTree);
            int startIndex = 0;
            while (startIndex < inputText.length()) {
                int endIndex = startIndex + encodedCharsLength >= inputText.length()
                        ? inputText.length() : startIndex + encodedCharsLength;
                String charsToEncode = inputText.substring(startIndex, endIndex);
                if (charsToEncode.length() < encodedCharsLength) {
                    for (int i = 0; i < encodedCharsLength - charsToEncode.length(); i++) {
                        charsToEncode += '\0';
                    }
                }
                char[] encoded = huffmanEncoder.getCode(charsToEncode);
                for (char encodedBit : encoded) {
                    bitOutputStream.writeBit(encodedBit == '0' ? 0 : 1);
                }
                startIndex += encodedCharsLength;
            }
        }

        try (BitInputStream bitInputStream = new BitInputStream(new BufferedInputStream(new FileInputStream(outputFileName)))) {
            int encodedCharactersLength = bitInputStream.readByte();
            System.out.println(encodedCharactersLength);
            HuffmanTree huffmanTree = HuffmanTree.loadTree(encodedCharactersLength, bitInputStream);
            System.out.println("Loaded huffman tree:");
            System.out.println(huffmanTree.getRootNode());

            int outputTextLength = bitInputStream.readInt();

            System.out.println("Decoded text:");
            try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream("decoded.txt"))) {
                for (int i = 0; i < outputTextLength; i += encodedCharactersLength) {
                    Node<Map.Entry<String, Integer>> node = huffmanTree.getRootNode();
                    while (!node.isLeaf()) {
                        int bit = bitInputStream.readBit();
                        if (bit == 0) {
                            node = node.getLeft();
                        } else {
                            node = node.getRight();
                        }
                    }
                    String decodedText = node.getItem().getKey();
                    while (decodedText.endsWith("\0")) {
                        decodedText = decodedText.substring(0, decodedText.length() - 1);
                    }
                    for (char decodedChar : decodedText.toCharArray()) {
                        outputStream.write(decodedChar);
                    }
                    System.out.print(decodedText);
                }
            }
        }
    }

    private static Triple<Integer, String, String> parseArguments(String[] args) {
        Preconditions.checkArgument(args.length == 3);
        int codedStringLength = Integer.parseInt(args[0]);
        return new ImmutableTriple<>(codedStringLength, args[1], args[2]);
    }
}