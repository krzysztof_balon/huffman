package algo.huffman;

import com.google.common.collect.Maps;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class HuffmanGrouper {
    public Set<Map.Entry<String, Integer>> group(int codedStringLength, String inputText) throws IOException {
        Map<String, Integer> charsGroupsFrequencies = Maps.newHashMap();

        int startIndex = 0;
        while (startIndex < inputText.length()) {
            int endIndex = startIndex + codedStringLength >= inputText.length()
                    ? inputText.length() : startIndex + codedStringLength;
            String textPartition = inputText.substring(startIndex, endIndex);
            if (textPartition.length() < codedStringLength) {
                for (int i = 0; i < codedStringLength - textPartition.length(); i++) {
                    textPartition += '\0';
                }
            }
            Integer count = charsGroupsFrequencies.getOrDefault(textPartition, 0);
            charsGroupsFrequencies.put(textPartition, ++count);
            startIndex += codedStringLength;
        }

        return charsGroupsFrequencies.entrySet();
    }
}
