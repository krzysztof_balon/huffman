package algo.huffman;

import com.google.common.base.Preconditions;

import java.io.IOException;
import java.io.InputStream;

public class BitInputStream implements AutoCloseable {
    private InputStream inputStream;
    private int nextBits;
    private int numBitsRemaining;
    private boolean isEndOfStream;

    public BitInputStream(InputStream inputStream) {
        Preconditions.checkNotNull(inputStream);
        this.inputStream = inputStream;
        numBitsRemaining = 0;
        isEndOfStream = false;
    }

    public int readBit() throws IOException {
        if (isEndOfStream) {
            return -1;
        }
        if (numBitsRemaining == 0) {
            nextBits = inputStream.read();
            if (nextBits == -1) {
                isEndOfStream = true;
                return -1;
            }
            numBitsRemaining = 8;
        }
        numBitsRemaining--;
        return (nextBits >>> numBitsRemaining) & 1;
    }

    public int readInt() throws IOException {
        int multiplier = 16777216;
        int integer = 0;
        for (int i = 0; i < 4; i++) {
            integer += multiplier * readByte();
            multiplier /= 256;
        }
        return integer;
    }

    public int readByte() throws IOException {
        return readCharacter();
    }

    public int readCharacter() throws IOException {
        int multiplier = 128;
        int character = 0;
        for (int i = 0; i < 8; i++) {
            character += multiplier * readBit();
            multiplier /= 2;
        }
        return character;
    }

    public void close() throws IOException {
        inputStream.close();
    }
}
