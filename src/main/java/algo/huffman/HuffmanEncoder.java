package algo.huffman;

import com.google.common.collect.Maps;

import java.util.Map;

public class HuffmanEncoder {
    private final Map<String, char[]> textToCodeMap;

    public HuffmanEncoder(HuffmanTree huffmanTree) {
        textToCodeMap = Maps.newHashMap();
        String code = "";
        fillTextToCodeMap(huffmanTree.getRootNode(), code);
    }

    private void fillTextToCodeMap(Node<Map.Entry<String, Integer>> node, String code) {
        if (!node.isLeaf()) {
            fillTextToCodeMap(node.getLeft(), code + '0');
            fillTextToCodeMap(node.getRight(), code + '1');
        } else {
            textToCodeMap.put(node.getItem().getKey(), code.toCharArray());
        }
    }

    public char[] getCode(String text) {
        return textToCodeMap.get(text);
    }
}
